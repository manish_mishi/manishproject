@extends('templetes.header')

@section('content')
<div class="container">

  <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel" id="myModal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="gridSystemModalLabel">Edit Post</h4>
        </div>
        <div class="modal-body">
          {!! Form::open(array('route'=>'updatePost','class'=>'form')) !!}
          <div class="jumbotron">
            <!-- <h1>{{ Auth::user()->name }}, You are logged in!</h1> -->
            <textarea rows="5" cols="50" name="edit_post" class="form-control" placeholder="What's your post today!" id="mEdit"></textarea>
            <input type="hidden" value="" name="post_id" id="postId">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
        {!! Form::close() !!}
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

  <div class="row">
    <div class="col-md-10 col-md-offset-1">
     <div class="panel panel-default">
       <!-- 	<div class="panel-heading">Home</div> -->

       <div class="panel-body">
         <div class="row row-offcanvas row-offcanvas-right">

          <div class="col-xs-12 col-sm-9">
            {!! Form::open(array('route'=>'storPost','class'=>'form')) !!}
            <div class="jumbotron">
              <!-- <h1>{{ Auth::user()->name }}, You are logged in!</h1> -->
              <textarea rows="5" cols="50" name="post" class="form-control" placeholder="What's your post today!"></textarea>
              <input type="hidden" value="{{ Auth::user()->id }}" name="user_id">
              <button type="submit" class="btn btn-md btn-primary pull-right" style="margin-top:1%;">Post</button>
            </div>
            {!! Form::close() !!}

            <div class="row">
              @foreach($postDetails as $post)
              <div class="panel panel-default" style="margin-left:1%;">
                <p style="margin-left:1%;margin-top:1%;">{{$post->post}}</p>
                <br>
                <button type="button" class="btn btn-xs btn-info" style="margin-top:1%;margin-bottom:1%;margin-left:1%;margin-right:1%;" data-toggle="modal" data-target="#myModal" value="{{$post->id}}" id="edit{{$post->id}}">Edit</button>
                <a href="{{url("post/delete",[$post->id])}}"><button type="button" class="btn btn-xs btn-danger" style="margin-top:1%;margin-bottom:1%;margin-left:1%;margin-right:1%;">Delete</button></a>
                <br>
              </div>

              <script type="text/javascript">
              $( document ).ready(function() 
              {

                  $("#edit<?php echo $post->id?>").click(function() //get details of membership package
                  {
                    var post_id = $(this).val();     

                    $.ajax({
                      method: "GET",
                      url: "post/edit/" + post_id,
                      success : function(data)
                      { 
                        $("#mEdit").val(data['0']['post']);
                        $("#postId").val(data['0']['id']);
                      }
                      
                    })

                  });
              });
              </script>

              @endforeach
            </div><!--/row-->
          </div><!--/.col-xs-12.col-sm-9-->
        </div><!--/row-->

        <hr>

      </div>
    </div>
  </div>
</div>

</div>
@endsection

