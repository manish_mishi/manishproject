<?php namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

use App\Post;
use DB;

class HomeController extends Controller {

	
	public function __construct()
	{
		
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('auth.login');
	}

	public function success()
	{
		$userId = Auth::user()->id;

	//fetching post tables details
		$postDetails = DB::table('posts') 
		->where('user_id',$userId)
		->get();

		return view('home',compact('postDetails'));
	}

	public function storePost(Request $request)
	{
		$post = new Post;
		$post->post = $request->get('post');
		$post->user_id = $request->get('user_id');
		$post->save();

		return redirect()->action('HomeController@success');
	}

	public function updatePost(Request $request)
	{
		$postId = $request->get('post_id');
		$post = $request->get('edit_post');

		DB::table('posts')
			->where('id', $postId)
			->update(['post' => $post]);

		return redirect()->action('HomeController@success');
	}

	public function deletePost($postId)
	{
		DB::table('posts')
			->where('id', $postId)
			->delete();

		return redirect()->action('HomeController@success');
	}


//Ajax Function

	public function getPost($postId)
	{
		$post = DB::table('posts') 
		->where('id', $postId)
		->select('*')
		->get();

		return $post;
	}

}
