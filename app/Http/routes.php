<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', [
	'uses' => 'HomeController@index', 
	'as' => 'home'
]);

Route::get('home', [
	'uses' => 'HomeController@success', 
	'as' => 'success'
]);

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

Route::post('store/post', array('as'=>'storPost','uses'=>'HomeController@storePost'));
Route::post('store/post/update', array('as'=>'updatePost','uses'=>'HomeController@updatePost'));
Route::get('post/delete/{post_id}', 'HomeController@deletePost');

//Ajax Route Function
Route::get('post/edit/{post_id}', 'HomeController@getPost');